from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'story4/homeku.html')

def about(request):
    return render(request, 'story4/about.html')

def experiences(request):
    return render(request, 'story4/experiences.html')

def abilities(request):
    return render(request, 'story4/abilities.html')

def hobby(request):
    return render(request, 'story4/Hobby.html')

def tes(request):
    return render(request, 'story1/tes.html')