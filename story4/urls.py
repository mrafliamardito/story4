from django.urls import path

from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.home, name='homeku'),
    path('about',views.about,name = 'about'),
    path('experiences',views.experiences, name = 'experiences'),
    path('abilities',views.abilities,name = 'abilities'),
    path('hobby',views.hobby,name = 'Hobby'),
    path('story1',views.tes,name = 'tes')
]